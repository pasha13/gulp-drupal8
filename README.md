# README #
### Drupal 8 Gulp for Theme Development ###

#### Prerequisites ####

* Drupal 8 project installed locally via Acquia 
* Node, Gulp.js, and NPM
* Module Browsersync downloaded, copied into MODULES folder; activated and install in Admin mode
* Terminal and IDE

#### 1. Global Settings for Local Dev ####
1.1. mv sites/example.settings.local.php to sites/default/settings.local.php

1.2. uncomment in settings.local.php 
* `$settings['cache']['bins']['render'] = 'cache.backend.null'` and 
* `$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null'`

1.3. uncomment in sites/default/settings.php
* `if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
     include $app_root . '/' . $site_path . '/settings.local.php'; }`
     
1.4. add to sites/development.services.yml `parameters: twig.config: debug:true auto_reload:true cache: false`

#### 2. Inside Theme: Configure Gulp and Folders ####
2.1. Create SRC dir with the structure repeating custom theme;

2.2. Rename CSS folder in SASS or SCSS; rename css files into _*.scss

2.3. Run `npm i` 

2.4. Adjust in Gulpfile.js `port` and `proxy` 

2.5. Adjust names of output files in [theme-name].info.yml and [theme-name].libraries.yml

### 3. Run GULP ###
* Browser should automatically open a browser window

#### Contrib ####

for the example the custom theme Drupa 8 Custom Theme was used 