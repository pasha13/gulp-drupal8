var gulp        = require('gulp'),
    browserSync = require('browser-sync'),
    sass        = require('gulp-sass'),
    prefix      = require('gulp-autoprefixer'),
    concat      = require('gulp-concat'),
    babel       = require('gulp-babel'),
    sourcemaps = require('gulp-sourcemaps'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    cp          = require('child_process');
// Change as required
var prxy = "http://drupal_gulp_3.dd:8083",
    port = "8083";
// tasks
gulp.task('browser-sync', ['sass', 'scripts', 'twig'], function() {
    browserSync.init({
        proxy: prxy,
        port: port
    });
});
gulp.task('imagemin', function () {
    return gulp.src('./src/images/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('./images'));
});

gulp.task('sass', function () {
    return gulp.src('./src/scss/style.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(prefix(['last 3 versions', '> 1%', 'ie 8'], { cascade: true }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('css'))
        .pipe(browserSync.reload({stream:true}))
});

gulp.task('scripts', function() {
    return gulp.src(['./src/js/*.js', './src/js/custom.js'])
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest('js'))
        .pipe(browserSync.reload({stream:true}))
});

gulp.task('twig', function(){
    gulp.src("src/templates/**/*.html.twig")
        .pipe(gulp.dest('./templates'));
});

gulp.task('clearcache', function(done) {
    return cp.spawn('drush', ['cache-rebuild'], {stdio: 'inherit'})
        .on('close', done);
});

gulp.task('reload', ['clearcache'], function () {
    browserSync.reload();
});

gulp.task('watch', function () {
    gulp.watch(['src/scss/*.scss', 'src/sass/**/*.scss'], ['sass']);
    gulp.watch(['src/js/*.js'], ['scripts']);
    gulp.watch(['src/images/*'], ['imagemin']);
    gulp.watch(['src/templates/**/*.html.twig'], ['twig']);
    gulp.watch(['templates/*.html.twig', '**/*.yml'], ['reload']);
});

gulp.task('default', ['browser-sync', 'watch']);
